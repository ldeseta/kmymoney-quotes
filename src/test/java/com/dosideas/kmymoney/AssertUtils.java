package com.dosideas.kmymoney;

import com.dosideas.kmymoney.domain.Quote;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AssertUtils {
    public static void assertQuote(Quote quote) {
        System.out.println(quote);
        assertNotNull(quote);
        assertNotNull(quote.getDate());
        assertNotNull(quote.getPrice());
        assertTrue(quote.getPrice().doubleValue() > 0);
    }
}
