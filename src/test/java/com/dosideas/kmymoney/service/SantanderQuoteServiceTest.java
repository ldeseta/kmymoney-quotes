package com.dosideas.kmymoney.service;

import static com.dosideas.kmymoney.AssertUtils.assertQuote;
import com.dosideas.kmymoney.domain.Quote;
import java.io.IOException;
import java.util.NoSuchElementException;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SantanderQuoteServiceTest {

    @Autowired
    private SantanderQuoteService quoteService;

    @ParameterizedTest
    @ValueSource(strings = {"SUPERGESTION MIX VI CUOTA A", "SUPERFONDO RENTA FIJA DOLARES CUOTA A", "SUPER AHORRO $ CUOTA A"})
    public void getQuote_fund_returnsQuote(String quoteCode) throws IOException {
        Quote quote = quoteService.getQuote(quoteCode).get();
        assertQuote(quote);
    }

    @Test
    public void getQuote_invalid_throwsNoSuchElementException() throws IOException {
        assertThrows(NoSuchElementException.class, () -> quoteService.getQuote("INVALID_QUOTE").get());
    }

}
