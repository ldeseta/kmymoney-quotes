package com.dosideas.kmymoney.service;

import static com.dosideas.kmymoney.AssertUtils.assertQuote;
import com.dosideas.kmymoney.domain.Quote;
import java.io.IOException;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AmbitoFinancieroQuoteServiceTest {

    @Autowired
    private AmbitoFinancieroQuoteService quoteService;

    @ParameterizedTest
    @ValueSource(strings = {"USDARS", "ARSUSD"})
    public void getQuote_pair_returnsQuote(String quoteCode) throws IOException {
        Quote quote = quoteService.getQuote(quoteCode).get();
        assertQuote(quote);
    }

}
