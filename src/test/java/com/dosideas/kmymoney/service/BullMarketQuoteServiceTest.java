package com.dosideas.kmymoney.service;

import static com.dosideas.kmymoney.AssertUtils.assertQuote;
import com.dosideas.kmymoney.domain.Quote;
import java.io.IOException;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class BullMarketQuoteServiceTest {

    @Autowired
    private BullMarketQuoteService quoteService;

    @ParameterizedTest
    @ValueSource(strings = {"BMACTAI", "ALDOPLA", "RJDAC2A", "QUAHDOA"})
    public void getQuote_hasQuotes_returnsQuote(String coin) throws IOException {
        System.out.println("Searching quote for " + coin);
        Quote quote = quoteService.getQuote(coin).get();
        assertQuote(quote);
    }

}
