package com.dosideas.kmymoney.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Quote {
    private LocalDate date;
    private BigDecimal price;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setPrice(String price) {
        this.price = new BigDecimal(price);
    }

    @Override
    public String toString() {
        return "{\"date\":\"" + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "\", \"price\":" + price + "}";
    }

}
