package com.dosideas.kmymoney.service;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.util.HttpUtils;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AmbitoFinancieroQuoteService {

    private static final String PATTERN_PRICE = ",\"([\\d,]*)\"";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final HttpUtils httpUtils;

    public AmbitoFinancieroQuoteService(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    public Optional<Quote> getQuote(String coinCode) throws IOException {
        switch (coinCode) {
            case "USDARS":
                return usdars();
            case "ARSUSD":
                return arsusd();
            default:
                throw new IllegalArgumentException("Unknown quote: " + coinCode);
        }
    }

    private Optional<Quote> usdars() throws IOException {
        LocalDate now = LocalDate.now();
        String fromDate = DATE_FORMATTER.format(now.minusDays(7)); //por si es fin de semana o feriado, ir mas dias atras
        String toDate = DATE_FORMATTER.format(now.plusDays(1));

        String body = httpUtils.getBodyFromUrl("https://mercados.ambito.com/dolarrava/mep/historico-general/" + fromDate + "/" + toDate, "mercados.ambito.com");
        Quote quote = new Quote();
        String patternString = PATTERN_PRICE;
        Matcher matcherPrice = Pattern.compile(patternString, Pattern.DOTALL).matcher(body);
        if (matcherPrice.find()) {
            quote.setPrice(matcherPrice.group(1).replaceAll(",", "."));
        } else {
            log.warn("No se encontró el precio. Body a continuacion", body);
            return Optional.empty();
        }

        quote.setDate(LocalDate.now());

        return Optional.of(quote);
    }

    private Optional<Quote> arsusd() throws IOException {
        BigDecimal priceUsdArs = usdars().get().getPrice();
        BigDecimal priceArsUsd = BigDecimal.ONE.divide(priceUsdArs, 8, RoundingMode.HALF_UP);
        Quote quote = new Quote();
        quote.setPrice(priceArsUsd);
        quote.setDate(LocalDate.now());
        return Optional.of(quote);
    }
}
