package com.dosideas.kmymoney.service;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.util.HttpUtils;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BullMarketQuoteService {

    private static final String PATTERN_PRICE = "\"fundBloombergId\":\"${QUOTECODE}\"[A-zÀ-ú,.:\\w\\s\\-\"]*?\"lastPrice\":([\\d\\.]*)";
    private final HttpUtils httpUtils;

    public BullMarketQuoteService(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    public Optional<Quote> getQuote(String coinCode) throws IOException {
        String body = httpUtils.getBodyFromUrl("https://www.bullmarketbrokers.com/operations/funds/GetFundsProducts?sortColumn=YearPercent&isAscending=false&isActive=true&pageSize=500&searchFundName=&searchCurrency=-1&searchFocus=-1&searchStrategy=&searchHorizon=-1&searchProfile=-1&searchMinInvestment=", "www.bullmarketbrokers.com");
        Quote quote = new Quote();
        String patternString = PATTERN_PRICE.replace("${QUOTECODE}", coinCode);
        Matcher matcherPrice = Pattern.compile(patternString, Pattern.DOTALL).matcher(body);
        if (matcherPrice.find()) {
            quote.setPrice(matcherPrice.group(1));
        } else {
            log.warn("No se encontró el precio. Body a continuacion", body);
            return Optional.empty();
        }

        quote.setDate(LocalDate.now());

        return Optional.of(quote);
    }

}
