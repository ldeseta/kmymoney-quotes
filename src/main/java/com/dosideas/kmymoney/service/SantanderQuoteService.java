package com.dosideas.kmymoney.service;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.util.HttpUtils;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SantanderQuoteService {

    private static final String PATTERN_PRICE = "${QUOTECODE}.*?<td align=\"right\" class=\"\">([\\d.,]*)<\\/td>";
    private static final Pattern PATTERN_DATE = Pattern.compile("Rendimientos al (\\d\\d\\/\\d\\d\\/\\d\\d\\d\\d)");
    private final HttpUtils httpUtils;

    public SantanderQuoteService(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    public Optional<Quote> getQuote(String code) throws IOException {
        String body = httpUtils.getBodyFromUrl("https://www.santanderrio.com.ar/ConectorPortalStore/Rendimiento", "www.santander.com.ar");
        Quote quote = new Quote();
        code = code.replaceAll("\\$", "\\\\\\$"); //escapar el $ para la regex
        String patternString = PATTERN_PRICE.replace("${QUOTECODE}", code);
        Matcher matcherPrice = Pattern.compile(patternString, Pattern.DOTALL).matcher(body);
        if (matcherPrice.find()) {
            quote.setPrice(matcherPrice.group(1).replace(".", "").replace(",", "."));
        } else {
            log.warn("No se encontró el precio. Body a continuacion", body);
            return Optional.empty();
        }

        Matcher matcherDate = PATTERN_DATE.matcher(body);
        if (matcherDate.find()) {
            quote.setDate(LocalDate.parse(matcherDate.group(1), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        } else {
            log.warn("No se encontró la fecha. Body a continuacion", body);
            return null;
        }

        return Optional.of(quote);
    }

}
