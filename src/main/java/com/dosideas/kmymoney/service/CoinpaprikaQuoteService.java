package com.dosideas.kmymoney.service;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.util.HttpUtils;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CoinpaprikaQuoteService {

    private static final String PATTERN_PRICE = "\"USD\":\\{\"price\":([\\d.]*)";

    private final HttpUtils httpUtils;
    private final RipioQuoteService ripioQuoteService;
    private final Map<String, String> apiCoinCodes;

    public CoinpaprikaQuoteService(RipioQuoteService ripioQuoteService, HttpUtils httpUtils) {
        this.ripioQuoteService = ripioQuoteService;
        this.httpUtils = httpUtils;
        apiCoinCodes = new HashMap<>();
        apiCoinCodes.put("ADA", "ada-cardano");
        apiCoinCodes.put("ALGO", "algo-algorand");
        apiCoinCodes.put("ATOM", "atom-cosmos");
        apiCoinCodes.put("BNB", "bnb-binance-coin");
        apiCoinCodes.put("BTC", "btc-bitcoin");
        apiCoinCodes.put("BUSD", "busd-binance-usd");
        apiCoinCodes.put("DOT", "dot-polkadot");
        apiCoinCodes.put("ETH", "eth-ethereum");
        apiCoinCodes.put("LDO", "ldo-lido-dao");
        apiCoinCodes.put("MATIC", "matic-polygon");
        apiCoinCodes.put("SOL", "sol-solana");
        apiCoinCodes.put("USDC", "usdc-usd-coin");
        apiCoinCodes.put("USDT", "usdt-tether");
    }

    public Optional<Quote> getQuote(String coinCode) throws IOException {
        coinCode = apiCoinCodes.get(coinCode);
        String body = httpUtils.getBodyFromUrl("https://api.coinpaprika.com/v1/tickers/" + coinCode + "?quotes=USD", "api.coinpaprika.com");
        Quote quote = new Quote();
        Matcher matcherPrice = Pattern.compile(PATTERN_PRICE, Pattern.DOTALL).matcher(body);
        if (matcherPrice.find()) {
            BigDecimal coinInUsd = new BigDecimal(matcherPrice.group(1));
            BigDecimal coinInArs = ripioQuoteService.usdToArs(coinInUsd);
            quote.setPrice(coinInArs);
        } else {
            log.warn("No se encontró el precio. Body a continuacion", body);
            return Optional.empty();
        }

        quote.setDate(LocalDate.now());

        return Optional.of(quote);
    }
}
