package com.dosideas.kmymoney.service;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.util.HttpUtils;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RipioQuoteService {

    private static final String PATTERN_PRICE = "${QUOTECODE}_ARS[\\dA-Za-z_:\",.]*sell_rate\":\"([\\d.]*)\"";
    private final HttpUtils httpUtils;

    public RipioQuoteService(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    public Optional<Quote> getQuote(String coinCode) throws IOException {
        String body = httpUtils.getBodyFromUrl("https://app.ripio.com/api/v3/rates/?country=AR", "app.ripio.com");
        Quote quote = new Quote();
        String patternString = PATTERN_PRICE.replace("${QUOTECODE}", coinCode);
        Matcher matcherPrice = Pattern.compile(patternString, Pattern.DOTALL).matcher(body);
        if (matcherPrice.find()) {
            quote.setPrice(matcherPrice.group(1));
        } else {
            log.warn("No se encontró el precio. Body a continuacion", body);
            return Optional.empty();
        }

        quote.setDate(LocalDate.now());

        return Optional.of(quote);
    }

    /** Converts a value in USD to ARS. */
    public BigDecimal usdToArs(BigDecimal usdPrice) throws IOException {
        BigDecimal usdcInArs = getQuote("USDC").get().getPrice();
        BigDecimal arsPrice = usdPrice.multiply(usdcInArs).setScale(4, RoundingMode.DOWN);
        return arsPrice;
    }
}
