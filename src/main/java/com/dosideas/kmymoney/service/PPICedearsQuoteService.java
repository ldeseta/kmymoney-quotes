package com.dosideas.kmymoney.service;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.util.HttpUtils;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PPICedearsQuoteService {

    private static final String PATTERN_PRICE = "\"ticker\":\"${QUOTECODE}\"[\\s\\d\\w,\":.\\n\\\\]*\\\"lastPrice\\\":([\\d.]*)";
    private final HttpUtils httpUtils;

    public PPICedearsQuoteService(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    public Optional<Quote> getQuote(String coinCode) throws IOException {
        String body = httpUtils.getBodyFromUrl("https://portfoliopersonal.com/Cotizaciones/Cedears", "portfoliopersonal.com");
        Quote quote = new Quote();
        String patternString = PATTERN_PRICE.replace("${QUOTECODE}", coinCode);
        Matcher matcherPrice = Pattern.compile(patternString, Pattern.DOTALL).matcher(body);
        if (matcherPrice.find()) {
            String price = matcherPrice.group(1);
            price = price.replaceAll("\\.", "").replaceAll(",", ".");
            quote.setPrice(price);
        } else {
            log.warn("No se encontró el precio. Body a continuacion", body);
            return Optional.empty();
        }

        quote.setDate(LocalDate.now());

        return Optional.of(quote);
    }

}
