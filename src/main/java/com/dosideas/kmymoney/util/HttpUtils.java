package com.dosideas.kmymoney.util;

import java.io.IOException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class HttpUtils {

    /**
     * Returns the content of a URL as String.
     *
     * @param url the URL to fetch.
     * @param host the Host header param. Some sites validate this field.
     * @return the content of the URL as String.
     */
    @Cacheable("urls")
    public String getBodyFromUrl(String url, String host) throws IOException {
        HttpResponse result = HttpRequest.get(url)
                .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:121.0) Gecko/20100101 Firefox/121.0")
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")
                .header("Accept-Language", "es-AR,es;q=0.8,en-US;q=0.5,en;q=0.3")
                //                .header("Accept-Encoding", "gzip, deflate, br")
                .header("Host", host)
//                .header("DNT", "1")
                .header("Connection", "keep-alive")
                .header("Upgrade-Insecure-Requests", "1")
                .send();

        if (result.statusCode() == 200) {
            return result.bodyText();
        } else {
//            Runtime.getRuntime().exec("\"c:\\Program Files\\Mozilla Firefox\\firefox.exe\" " + url);
            throw new IOException("[ERROR] Status: " + result.statusCode() + " al conectarse a " + url);
        }
    }

}
