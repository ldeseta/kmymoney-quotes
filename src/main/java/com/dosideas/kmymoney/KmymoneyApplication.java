package com.dosideas.kmymoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class KmymoneyApplication {

    public static void main(String[] args) {
        SpringApplication.run(KmymoneyApplication.class, args);
    }
}
