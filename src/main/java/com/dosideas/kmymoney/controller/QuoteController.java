package com.dosideas.kmymoney.controller;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.service.AmbitoFinancieroQuoteService;
import com.dosideas.kmymoney.service.BancoPianoCedearsQuoteService;
import com.dosideas.kmymoney.service.BullMarketQuoteService;
import com.dosideas.kmymoney.service.CoinpaprikaQuoteService;
import com.dosideas.kmymoney.service.PPICedearsQuoteService;
import com.dosideas.kmymoney.service.RipioQuoteService;
import com.dosideas.kmymoney.service.SantanderQuoteService;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class QuoteController {

    private final AmbitoFinancieroQuoteService ambitoFinancieroQuoteService;
    private final SantanderQuoteService santanderQuoteService;
    private final RipioQuoteService ripioQuoteService;
    private final CoinpaprikaQuoteService coinPaprikaQuoteService;
    private final BullMarketQuoteService bullMarketQuoteService;
    private final BancoPianoCedearsQuoteService bancoPianoCedearsQuoteService;
    private final PPICedearsQuoteService ppiCedearsQuoteService;

    @GetMapping("/quote/santander/{code}")
    public Quote getSantanderQuote(@PathVariable String code) throws IOException {
        return santanderQuoteService.getQuote(code).get();
    }

    @GetMapping("/quote/ripio/{coin}")
    public Quote getRipioQuote(@PathVariable String coin) throws IOException {
        return ripioQuoteService.getQuote(coin).get();
    }

    @GetMapping("/quote/coinpaprika/ARS/{coin}")
    public Quote getCoinpaprikaQuote(@PathVariable String coin) throws IOException {
        return coinPaprikaQuoteService.getQuote(coin).get();
    }

    @GetMapping("/quote/bullmarket/{code}")
    public Quote getBullMarketQuote(@PathVariable String code) throws IOException {
        return bullMarketQuoteService.getQuote(code).get();
    }

    @GetMapping("/quote/bancopiano/cedears/{code}")
    public Quote getBancoPianoCedearsQuote(@PathVariable String code) throws IOException {
        return bancoPianoCedearsQuoteService.getQuote(code).get();
    }

    @GetMapping("/quote/ppi/cedears/{code}")
    public Quote getPPICedearsQuote(@PathVariable String code) throws IOException {
        return ppiCedearsQuoteService.getQuote(code).get();
    }

    @GetMapping("/quote/ambitofinanciero/dolar/mep/{code}")
    public Quote getAmbitoFinancieroDolarQuote(@PathVariable String code) throws IOException {
        return ambitoFinancieroQuoteService.getQuote(code).get();
    }

}
