# Configuración de KMyMoney Quotes
KMyMoney Quotes has Web REST services to provider online quotes, to use with
KMyMoney.

## Setup
- Install KMyMoney 5.x
- Locate and open KMyMoney configuration file: C:\Users\<USER>\AppData\Local\kmymoney\kmymoneyrc
- Add the following providers (you can also manually add these providers by
opening KMyMoney and goingt to Settings > Configure KMyMoney... > Online Quotes)

```
[Online-Quote-Source-KQuotes Cointelegraph en ARS]
CSVURL=
DateFormatRegex=%y-%m-%d
DateRegex="date":"(.*)",
IDBy=0
IDRegex=
PriceRegex="price":([\\d.]*)
SkipStripping=true
URL=http://localhost:8787/quote/cointelegraph/ARS/%1

[Online-Quote-Source-KQuotes Ripio en ARS]
CSVURL=
DateFormatRegex=%y-%m-%d
DateRegex="date":"(.*)",
IDBy=0
IDRegex=
PriceRegex="price":([\\d.]*)
SkipStripping=true
URL=http://localhost:8787/quote/ripio/%1

[Online-Quote-Source-KQuotes Santander]
CSVURL=
DateFormatRegex=%y-%m-%d
DateRegex="date":"(.*)",
IDBy=0
IDRegex=
PriceRegex="price":([\\d.]*)
SkipStripping=true
URL=http://localhost:8787/quote/santander/%1
```